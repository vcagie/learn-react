import React from 'react';
import Card from './Card';

const CardList = ({ robots }) => {
    // if(true){
    //     throw new Error('NOOO');
    // }
    return (
        <div>
            {
                robots.map((user, i) => {
                    return (
                        <Card 
                        key={i} 
                        id={robots[i].id} 
                        name={robots[i].nama}
                        position={robots[i].position} 
                        keunggulan={robots[i].keunggulan}
                        />
                    );
                })
            }
        </div> 
    );
}

export default CardList;
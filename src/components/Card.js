import React from 'react';

const Card = (props) =>{
    const {name, position, keunggulan} = props;
    return (
        <div className='bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5 w-25'> 
            <img alt='robots' src={'https://robohash.org/'+name+'?size=200x200'}/> 
            <div>
                <h2>Name :{name}</h2>
                <p>Posisi: {position}</p>
                <p>Kemampuan: {keunggulan}</p>
            </div>
        </div> 
    );
}

export default Card;